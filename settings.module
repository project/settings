<?php

/**
 * Get all settings for the given module.
 * 
 * @param $module
 *   The name of the module.
 * @return
 *   Array of module settings.      
 */
function settings_get_settings($module) {
  global $settings;

  if (!array_key_exists($module, $settings) {
    //$settings[$module] = module_invoke($module, 'get_settings');
    $settings[$module] = call_user_func($module . '_' . 'get_settings'); // for use at bootstrap
  }

  return $settings[$module];
}

/**
 * Check the existance of the given module setting.
 * 
 * @param $module
 *   The name of the module.
 * @param $name
 *   The name of the setting.
 * @return
 *   TRUE - if the given setting exists, otherwise will trigger a warning.      
 */
function settings_check($module, $name) {
  $module_settings = settings_get_settings($module);

  if (isset($module_settings[$name])) {
    return TRUE;
  }
  else {
    trigger_error(t('Setting %name in module %module is not exist.', array('%name' => $name, '%module' => $module)), E_USER_WARNING);
  }
}

/**
 * Get current value of the given setting or default value (or specified value) if the setting is not set.
 * 
 * @param $module
 *   The name of the module.
 * @param $name
 *   The name of the setting.
 * @param $value
 *   Optional. The value returned if the setting is not set. If not specified defined default value will be returned. 
 * @return
 *   The current value of the setting or default value (or specified value) if the setting is not set.      
 */
function settings_get($module, $name, $value = NULL) {
  $module_setting = _settings_get_item($module, $name);
  if (isset($module_setting)) {
    if (!isset($value)) {
      $value = $module_setting['#default'];
    }
    return variable_get($module.'_'._settings_join_names($name), $value);
  }
}

/**
 * Set current value of the given setting.
 * 
 * @param $module
 *   The name of the module.
 * @param $name
 *   The name of the setting.
 * @param $value
 *   The value to be set. 
 */
function settings_set($module, $name, $value) {
  $module_setting = _settings_get_item($module, $name);
  if (isset($module_setting)) {
    variable_set($module.'_'._settings_join_names($name), $value);
  }
}

/**
 * Get current values of all settings for the given module or default values (or specified values) if the particular setting is not set.
 * 
 * @param $module
 *   The name of the module.
 * @param $values
 *   Optional. The array of values returned if the particular setting is not set. If not specified defined default values will be returned. 
 * @return
 *   Current values  or default values (or specified values) if the particular setting is not set.      
 */
//TODO: implement
/*function settings_get_all($module, $values = array()) {
  global $conf;

  $result = array();
  $module_settings = settings_get_settings($module);
  foreach ($module_settings as $name => $default_value) {
    $result[$name] = (array_key_exists($module.'_'.$name, $conf) ? $conf[$module.'_'.$name] : (array_key_exists($name, $values) ? $values[$name] : $default_value));
  }
  return $result;
}*/

/**
 * Delete all module settings. So, defined default values will be used as current values.  
 * 
 * @param $module
 *   The name of the module.
 */
function settings_delete_all($module) {
  $module_settings = settings_get_settings($module);
  foreach (array_keys($module_settings) as $name) {
    variable_del($module.'_'.$name);
  }
}

/**
 * Prepare settings form by specifing "#default_value" values and rename element names from "<setting_name>" to "<module_name> '_' <setting_name>".
 * 
 * @param $module
 *   The name of the module.
 * @param $form
 *   The settings form to be prepared.
 * @return
 *   The prepared settings form.
 *   
 * @todo
 *   Use "element_children()" (?).        
 */
function settings_prepare_settings_form($module, $form) {
  $result_form = $form;
  foreach ($form as $name => $element) {
    // If this is a form element
    if (($name{0} != '#') && is_array($element)) {
      // If this is an input form element
      if (in_array($element['#type'], array('checkbox', 'checkboxes', 'date', 'file', 'password', 'radio', 'radios', 'select', 'textarea', 'textfield', 'weight', 'hidden'))) {
        // If "default_value" applicable
        if (!in_array($element['#type'], array('file', 'password'))) {
          $element['#default_value'] = settings_get($module, $name);
        }

        // Rename element name in result form
        $result_form[$module.'_'.$name] = $element;
        unset($result_form[$name]);
      }
      else {
        $result_form[$name] = settings_prepare_settings_form($module, $element);
      }
    }
  }
  return $result_form;
}

function _settings_prepare_names($names) {
  if (is_scalar($names)) {
    return array($names);
  }
  else {
    return $names;
  }
}

function _settings_join_names($names) {
  return join('_', _settings_prepare_names($names));
}

function &_settings_get_item($module, $names) {
  $settings = settings_get_settings($module); 
  
  $names = _settings_prepare_names($names);
  
  $i = 0; 
  while ($i <= count($names) - 1) {
    $type = $settings['#type'];
    $name = $names[$i];
    switch ($type) {
      case 'list':
        if ($name == '[]') {
          $settings =& $settings['#item'];
        }
        else {
          $settings = NULL;
        }  
        break;
      case 'struct':
        $settings =& $settings[$name];
        break;
      default:
        $settings = NULL;  
    }
    
    if (!isset($settings)) {
      trigger_error(t('Setting %name in module %module is not exist.', array('%name' => join('->', $names), '%module' => $module)), E_USER_WARNING);
      return NULL;
    }
    
    $i++;
  }
  
  return &$settings;
}
