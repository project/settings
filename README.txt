This module is only provide API for other modules.

FEATURES

This module allows for other modules:
- To specify structure and default values of all module settings in one place: "hook_get_settings()".
- Use variable naming standards: <module_name> "_" <setting_name>.
- Use defined default values everywhere (with option to override default value)
  Example:
    Instead of "variable_get('my_module_var1', 456)" in several places of a code, you can just use "settings_get('my_module', 'var1')".
    To override default value use the same approach as for "variable_get": "settings_get('my_module', 'var1', 987)".
- Automatic check for existence of setting at the moment of "set" or "get" and in case if setting it's not defined raise an error.
- Simply delete all module settings at "hook_uninstall()", just calling "settings_delete_all('my_module')".
  So, you will never forget about to delete some variable.
- Automatic specify default values ('#default_value') for admin settings form by using "settings_prepare_settings_form('my_module', $form)",
  this will also automatically rename element names from format '<setting_name>' to format '<module_name> "_" <setting_name>'
  (for proper processing with "system_settings_form_submit()").
 